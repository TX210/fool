var uuid = require('uuid');
var mysql = require('mysql2/promise');
var io = require('socket.io').listen(9090);
var bodyParser = require('body-parser');
var gameSocket;
var lobby = uuid.v4();
var table = [];
var rooms = [];

var games_count = 0;
mysql.createConnection({
    user: 'root'
    , password: 'ravenagefm'
    , database: 'socialchance,biz',
    socketPath: '/var/run/mysqld/mysqld.sock',
    namedPlaceholders: true,
    decimalNumbers: true
}).then(val => {
    pdo = val;
    users = new Users(pdo, io, ["login", "avatar", "chip", "fio"], {
        "/games/knb": {
            needOnline: true
        },
        "/lottery": {
            needOnline: false
        },
        "/finance/buy": {
            needOnline: false
        },
        "/chat": {
            needOnline: true
        },
        "/finance/history": {
            needOnline: false
        },
        "/finance/in": {
            needOnline: false
        },
        "/finance/out": {
            needOnline: false
        },
        "/games/checkers": {
            needOnline: true
        },
        "/games/fool": {
            needOnline: true
        }
    }, ["knb", "fool", /* "checkers"*/]);
});

function makegame(date) {
    this.date = date;
    this.id = '';
    this.users = [];
    this.winners = [];
    this.deck = [];
    this.cards = [[1, 6], [1, 7], [1, 8], [1, 9], [1, 10], [1, 11], [1, 12], [1, 13], [1, 14], [2, 6], [2, 7], [2, 8], [2, 9], [2, 10], [2, 11], [2, 12], [2, 13], [2, 14], [3, 6], [3, 7], [3, 8], [3, 9], [3, 10], [3, 11], [3, 12], [3, 13], [3, 14], [4, 6], [4, 7], [4, 8], [4, 9], [4, 10], [4, 11], [4, 12], [4, 13], [4, 14]];
    this.fight = [];
    this.trump = 0;
    this.skip = [];
    this.turn = '';
    this.taken = 0;
    this.countReady = 0
}
function makePlayer(id) {
    this.personalData = getUser(socket.uid);
    this.socketID = id;
    this.hand = [];
    this.mode = -1;
    this.ready = 0;
    this.variants = [];
}
function getWager(msg) {
    for (var i = 0; i < rooms.length; i++) {
        if (msg == rooms[i].id) {
            return table[i].wager;
        }
    }
}
function getRoomByTableId(id) {
    for (var i = 0; i <= rooms.length; i++) {
        if (rooms[i].id == id) {
            console.log(i);
            return i
        }
    }

}
function getRoomByUserId(id) {

    var roomslist = rooms.length;
    console.log('roomsList', roomslist);
    console.log('getRoomByUserId-----------------');
    for (var i = 0; i < roomslist; i++) {
        console.log('room#-->' + i);
        for (var j = 0; j < rooms[i].users.length; j++) {
            console.log('user#-->' + j);
            if (id == rooms[i].users[j].socketID) {
                console.log('roompos--->' + i);
                return i;

            }

        }
    }
    console.log('getRoomByUserId-----------------');
}
function getUserIdInRoom(id) {
    var roomslist = rooms.length;
    console.log('getUserIdInRoom-----------------');
    for (var i = 0; i < roomslist; i++) {
        console.log('room#-->' + i);
        for (var j = 0; j < rooms[i].users.length; j++) {
            console.log('user#-->' + j);
            if (id == rooms[i].users[j].socketID) {
                console.log('userpos--->' + i);
                return j;

            }

        }
    }
    console.log('getUserIdInRoom-----------------');
}
/*function getModebyUserID(roompos, userpos) {
 return rooms[roompos].users[userpos].mode
 }*/
io.of("/games/fool/").on('connection', function (socket) {
     var initLobby = function (sio, socket) {
        io = sio;
        console.log('socket id --->' + socket.id);
        socket.join(lobby);
        console.log('lobby ID --->' + lobby);
        gameSocket = socket;
        var usp = await
        users.getUser(socket.uid);
        var date = new Date();
        io.of('games/fool/').in(lobby).emit('Greet');
        gameSocket.emit('initLobby', 'init');
        if (table.length) {
            updateforNew(table);
        }
        gameSocket.on('disconnect', function () {
            console.log('user disconnected:' + this.id);
            /* var leave = this.id;
             if (rooms.length>0) {
             var leavedRoom = getRoomByUserId(leave);
             var leavepos = getUserIdInRoom(leave)
             console.log(leavedRoom);
             console.log(leavepos);
             rooms[leavedRoom].splice(leavepos, 1);
             if (rooms[leavedRoom].length == 0) {
             rooms.splice(leavedRoom, 1);
             table.splice(leavedRoom, 1);
             }
             console.log(rooms);
             console.log(table);
             UpdateTable(table)
             }
             */
        });
        gameSocket.on('hostCreateNewGame', hostCreateNewGame);
        gameSocket.on('JoinGame', JoinGame);
        gameSocket.on('playerReady', function (msgID) {
            console.log('INFO FORM CLIENT-->' + msgID);
            var roomID = getRoomByUserId(msgID);
            var userpos = getUserIdInRoom(msgID);
            console.log('ROOM ID IF PLAYER READY:--->' + roomID);
            rooms[roomID].users[userpos].ready = 1;
            rooms[roomID].countReady++;
            console.log(table[roomID]);
            /*for(var i=0; i<rooms[roomID].users[userpos].length;i++){
             if(rooms[roomID].users[i].ready == 1){

             }
             }
             */
            if (rooms[roomID].countReady == table[roomID].target) {
                console.log("GAME IS READY");
                initGame(roomID, socket.id);
                //todo Сделай инициализацию игры новым методом и разбраться со статусами на фронте
            } else {
                UpdatePreScreen(rooms[roomID], socket.id);
                console.log('Worked');

            }
            UpdatePreScreen(rooms[roomID], socket.id);
        });
        gameSocket.on('game_action', function (cardID, id) {
            //var id = socket.id;
            console.log('Current ID--->' + id);
            var roompos = getRoomByUserId(id);
            var userpos = getUserIdInRoom(id);
            console.log('roompos-->' + roompos);
            console.log('userpos-->' + userpos);
            var mode = rooms[roompos].users[userpos].mode;
            console.log('cardID--->' + cardID);
            console.log('Current card--->' + rooms[roompos].users[userpos].hand[cardID[1]]);
            var card = rooms[roompos].users[userpos].hand[cardID[1]];
            if (mode == 1) {
                console.log('Attack');
                console.log(rooms[roompos].users[userpos].mode);
                rooms[roompos].deck.push(card);
                rooms[roompos].fight.push(card);
                rooms[roompos].users[userpos].hand.splice(cardID[1], 1);
                rooms[roompos].users[userpos].mode = 2;
                console.log(rooms[roompos].users[userpos].mode);
                turnover(roompos, userpos);
            } else if (mode == 0) {
                console.log('Defend');
                console.log('fight --->' + rooms[roompos].fight[0]);
                console.log('fight[0] --->' + rooms[roompos].fight[0][0]);
                console.log('fight[1] --->' + rooms[roompos].fight[0][1]);
                if (card[0] == rooms[roompos].trump && rooms[roompos].fight[0] != rooms[roompos].trump) {
                    rooms[roompos].deck.push(card);
                    rooms[roompos].users[userpos].hand.splice(cardID[1], 1);
                    rooms[roompos].fight.length = 0;
                    turnover(roompos, userpos);
                } else if (card[0] == rooms[roompos].fight[0][0] && card[1] > rooms[roompos].fight[0][1]) {
                    rooms[roompos].deck.push(card);
                    rooms[roompos].users[userpos].hand.splice(cardID[1], 1);
                    rooms[roompos].fight.length = 0;
                    turnover(roompos, userpos);
                }
            } else if (mode == 2) {
                console.log('More cards');
                //if (checkMore(roompos, userpos) == true) {
                var deck_length = rooms[roompos].deck.length;

                if (rooms[roompos].taken == 1) {
                    for (var i = 0; i < deck_length; i++) {
                        if (card[1] == rooms[roompos].deck[i][1]) {
                            rooms[roompos].deck.push(card);
                            rooms[roompos].fight.push(card);
                            rooms[roompos].users[userpos].hand.splice(cardID[1], 1);
                            turnover(roompos, userpos);
                            console.log('Worked more card--->' + card);
                            console.log('Worked fight--->' + rooms[roompos].fight);
                            console.log('Worked deck--->' + rooms[roompos].deck);
                        }
                    }


                } else {
                    for (var i = 0; i < deck_length; i++) {
                        if (card[1] == rooms[roompos].deck[i][1]) {
                            rooms[roompos].deck.push(card);
                            rooms[roompos].fight.push(card);
                            rooms[roompos].users[userpos].hand.splice(cardID[1], 1);
                            defturn(roompos);
                            console.log('Worked more card--->' + card);
                            console.log('Worked fight--->' + rooms[roompos].fight);
                            console.log('Worked deck--->' + rooms[roompos].deck);
                        }
                    }
                }

                // }
                //else
                if (rooms[roompos].users.length == 2) {
                    rooms[roompos].fight.length = 0;
                    rooms[roompos].deck.length = 0;
                    swapModes(roompos);
                    refill(roompos);
                    turnover(roompos, userpos);
                }


            }
            if (checkfinish(roompos) != -1) {
                var winnerpos = rooms[roompos].winners.length;
                rooms[roompos].winners.push(checkfinish(roompos));
                rooms[roompos].users.splice(checkfinish(roompos), 1);
                if (winnerpos == 0) {
                    usp.chip += parseFloat(getWager(rooms[roompos].id));
                    users.updateUser(socket.uid);
                } else if (winnerpos == 1) {
                    usp.chip += parseFloat(getWager(rooms[roompos].id)) / 2;
                    users.updateUser(socket.uid);
                } else if (winnerpos == 2) {
                    usp.chip += parseFloat(getWager(rooms[roompos].id)) / 4;
                    users.updateUser(socket.uid);
                }
            }
            console.log(rooms[roompos].deck);
            console.log(rooms[roompos].fight);
            console.log(rooms[roompos].turn);
            UpdateGamebyID(roompos, socket.id);
        });
        gameSocket.on('take', function (id) {
            var roompos = getRoomByUserId(id);
            var userpos = getUserIdInRoom(id);
            console.log('Take');
            console.log('userpos-->' + userpos);
            console.log(rooms[roompos].deck);
            rooms[roompos].taken = 1;
            turnover(roompos, userpos);
            UpdateGamebyID(roompos, userpos);
            /*
             for (var i = 0; i < j; i++) {
             rooms[roompos].users[userpos].hand.push(rooms[roompos].deck[i]);
             }
             rooms[roompos].deck.length = 0;
             rooms[roompos].fight.length = 0;
             swapModes(roompos,'take');
             refill(roompos);
             attackturn(roompos);
             UpdateGamebyID(roompos, userpos);

             */
        });
        gameSocket.on('pass', function (id) {
            var roompos = getRoomByUserId(id);
            var userpos = getUserIdInRoom(id);
            console.log('Pass');
            rooms[roompos].skip.push(1);
            if (rooms[roompos].skip.length == rooms[roompos].users.length - 1) {
                if (rooms[roompos].taken == 0) {
                    console.log('Worked Pass');
                    rooms[roompos].deck.length = 0;
                    console.log('deck--->', rooms[roompos].deck)
                    rooms[roompos].fight.length = 0;
                    rooms[roompos].skip.length = 0
                    console.log('fight--->', rooms[roompos].fight)
                    swapModes(roompos, 'pass');
                    refill(roompos);
                    attackturn(roompos);
                } else if (rooms[roompos].taken == 1) {
                    for (var j = 0; j < rooms[roompos].users.length; j++) {
                        if (rooms[roompos].users[j].mode == 0) {
                            for (var i = 0; i < rooms[roompos].deck.length; i++) {
                                rooms[roompos].users[j].hand.push(rooms[roompos].deck[i]);
                            }

                        }
                    }
                    rooms[roompos].deck.length = 0;
                    rooms[roompos].fight.length = 0;
                    rooms[roompos].taken = 0;
                    swapModes(roompos, 'take');
                    refill(roompos);
                    attackturn(roompos);
                    UpdateGamebyID(roompos, userpos);
                }

            } else {
                turnover(roompos, userpos);
            }

            UpdateGamebyID(roompos, socket.id);
        });
        gameSocket.on('timeout', function (id) {
            var roompos = getRoomByUserId(id);
            var userpos = getUserIdInRoom(id);
            var mode = rooms[roompos].users[userpos].mode;
            if (mode == 0) {
                rooms[roompos].taken = 1;
                turnover(roompos, userpos);

            } else if (mode == 1) {
                var card = Math.floor(Math.random() * rooms[roompos].users[userpos].hand.length);
                rooms[roompos].deck.push(rooms[roompos].users[userpos].hand[card]);
                rooms[roompos].fight.push(rooms[roompos].users[userpos].hand[card]);
                rooms[roompos].users[userpos].hand.splice(card, 1);
                turnover(roompos, userpos)
            } else {
                turnover(roompos, userpos);
                rooms[roompos].skip.push(1);

            }
            UpdateGamebyID(roompos, id)
        });

        function JoinGame(msg) {

            if (parseFloat(usp.chip) - (parseFloat(getWager(msg)) < 0)) {
                socket.emit('money')
            } else {
                console.log(msg);
                socket.leave(lobby);
                socket.join(msg);
                var roompos = getRoomByTableId(msg);
                rooms[roompos].users.push(new makePlayer(socket.id));
                console.log(rooms);
                table[roompos].countPlayers += 1;
                console.log(table[roompos]);
                UpdateTable(table);
                /* if (table[roompos].target == rooms[roompos].users.length) {
                 initGame(roompos, socket.id);

                 }*/
                initPreScreen(rooms[roompos], socket.id);
                UpdatePreScreen(rooms[roompos], socket.id)
            }
        }

        function hostCreateNewGame(msg) {
            // Create a unique Socket.IO Room
            var GameId = ( Math.random() * 100000 ) | 0;
            console.log('NUM');
            console.log(GameId);
            socket.leave(lobby);
            if (msg.wager === undefined || isNaN(parseFloat(msg.wager)) === true || parseFloat(msg.wager) < 0.01) {
                socket.emit('noValue')
            } else if (parseFloat(usp.chip) - (parseFloat(msg.wager)) < 0) {
                socket.emit('money')
            } else if (msg.target === undefined || isNaN(parseFloat(msg.target)) === true || parseInt(msg.target) < 1) {
                socket.emit('noCountPlayer')
            } else {
                usp.chip -= parseFloat(msg.wager);
                users.updateUser(socket.uid);
                socket.join(GameId);
                rooms[games_count] = {};
                rooms[games_count] = new makegame(date);
                rooms[games_count].id = GameId;
                rooms[games_count].users.push(new makePlayer(socket.id));
                console.log('Obj')
                console.log(rooms[games_count]);
                msg.countPlayers = 1;
                msg.thisGameid = GameId;
                msg.game_num = games_count;
                table.push(msg);
                UpdateTable(table);
                io.of('games/fool/').to(GameId).emit('welcome', GameId);
                initPreScreen(rooms[games_count], socket.id);
                games_count += 1;
            }
        }

        function UpdateGamebyID(roompos, id) {
            var Gameroom = rooms[roompos];
            //io.of('games/fool/').in(rooms[rooompos].id).emit('UpdateGame', Gameroom, id)
            //checkVariants(roompos);
            for (var i = 0; i < rooms[roompos].users.length; i++) {
                io.of('games/fool/').to(rooms[roompos].users[i].socketID).emit('UpdateGame', rooms[roompos], rooms[roompos].users[i].socketID);
                console.log('socket id?--->' + rooms[roompos].users[i].socketID);
                console.log('turn-->' + rooms[roompos].turn);
            }
        }

        function updateforNew(table) {
            io.of('games/fool/').in(lobby).emit('updateTable', table);
            console.log('sended for new', table);
        }

        function UpdateTable(table) {
            io.of('games/fool/').emit('updateTable', table);
            console.log('sended table ', table);
        }

        function initPreScreen(room, id) {

            io.of('games/fool/').in(room.id).emit('PreScreen', room, id);
            console.log(room);
            console.log('sended Pre Screen');
        }

        function UpdatePreScreen(Gameroom, id) {
            for (var i = 0; i < Gameroom.users.length; i++) {
                io.of('games/fool/').to(Gameroom.users[i].socketID).emit('UpdatePreScreen', Gameroom, id, Gameroom.users[i].socketID);

            }
            console.log('GAMEROOM ');
            console.log(Gameroom);

        }

        function initGame(roompos, id) {
            drawcard(roompos);
            rooms[roompos].trump = Math.floor(Math.random() * 4 + 1);
            for (var i = 0; i < rooms[roompos].users.length; i++) {
                rooms[roompos].users[i].mode = 2;
            }
            var value = 15;


            for (var i = 0; i < rooms[roompos].users.length; i++) {
                for (var j = 0; j < 6; j++) {
                    if (rooms[roompos].users[i].hand[j][0] == rooms[roompos].trump && rooms[roompos].users[i].hand[j][1] < value) {
                        value = rooms[roompos].users[i].hand[j][1];
                        rooms[roompos].turn = rooms[roompos].users[i].socketID;
                        var first = i
                    }
                }

            }
            if (value = 15) {
                //todo добавить по одной карте
            }
            rooms[roompos].users[first].mode = 1;
            if (first == (rooms[roompos].users.length - 1)) {
                rooms[roompos].users[0].mode = 0;
            }
            else {
                rooms[roompos].users[first + 1].mode = 0
            }
            //io.of('games/fool/').in(rooms[roompos].id).emit('initGame', rooms[roompos], socket.id);
            for (var i = 0; i < rooms[roompos].users.length; i++) {
                io.of('games/fool/').to(rooms[roompos].users[i].socketID).emit('initGame', rooms[roompos], rooms[roompos].users[i].socketID)
                console.log('socket id?--->' + rooms[roompos].users[i].socketID);
            }

        }

        function drawcard(roompos) {
            for (var i = 0; i < rooms[roompos].users.length; i++) {
                console.log('iterator--->' + i);
                for (var j = 0; j <= 5; j++) {
                    var rand_card = Math.floor(Math.random() * rooms[roompos].cards.length);
                    console.log(rooms[roompos].users[i].hand);
                    rooms[roompos].users[i].hand.push(rooms[roompos].cards[rand_card]);
                    rooms[roompos].cards.splice(rand_card, 1)
                }
            }
        }

        function attackturn(roompos) {
            console.log('-----------Check--------');
            for (var i = 0; i < rooms[roompos].users.length; i++) {
                if (rooms[roompos].users[i].mode == 1) {
                    rooms[roompos].turn = rooms[roompos].users[i].socketID
                    console.log('turn--->' + rooms[roompos].users[i].socketID);
                    console.log('mode player who going--->' + rooms[roompos].users[i].mode);

                }
            }
        }

        function defturn(roompos) {
            for (var i = 0; i < rooms[roompos].users.length; i++) {
                if (rooms[roompos].users[i].mode == 0) {
                    rooms[roompos].turn = rooms[roompos].users[i].socketID
                    console.log('turn--->' + rooms[roompos].users[i].socketID);
                    console.log('mode player who DEFENDING--->' + rooms[roompos].users[i].mode);
                }
            }
        }

        function turnover(roompos, userpos) {
            console.log('turned');
            console.log('deck', rooms[roompos].deck);
            if (userpos < rooms[roompos].users.length - 1) {
                rooms[roompos].turn = rooms[roompos].users[userpos + 1].socketID;
                if (rooms[roompos].taken == 1 && rooms[roompos].users[userpos + 1].mode == 0) {
                    turnover(roompos, userpos + 1);
                }
            }
            else {
                rooms[roompos].turn = rooms[roompos].users[0].socketID;
                if (rooms[roompos].taken == 1 && rooms[roompos].users[0].mode == 0) {
                    turnover(roompos, 0);
                }
            }
        }

        function refill(roompos) {
            for (var i = 0; i < rooms[roompos].users.length; i++) {
                if (rooms[roompos].users[i].hand.length < 6 && rooms[roompos].cards.length != 0) {
                    while (rooms[roompos].users[i].hand.length <= 5) {
                        var rand_card = Math.floor(Math.random() * rooms[roompos].cards.length - 1);
                        console.log('rand_card' + rooms[roompos].cards[rand_card]);
                        rooms[roompos].users[i].hand.push(rooms[roompos].cards[rand_card]);
                        console.log('hand', rooms[roompos].users[i].hand);
                        rooms[roompos].cards.splice(rand_card, 1)
                    }
                }
            }
        }

        function checkfinish(roompos) {
            if (rooms[roompos].deck.length == 0) {
                for (var i = 0; i <= rooms[roompos].users.length; i++) {
                    if (rooms[roompos].users[i].hand.length == 0) {
                        return i
                    } else {
                        return -1
                    }
                }

            }
        }

        function checkMore(roompos, userpos) {
            for (var i = 0; i < rooms[roompos].users[userpos].hand.length; i++) {
                for (var j = 0; j < rooms[roompos].deck.length; j++) {
                    if (rooms[roompos].users[userpos].hand[i][1] == rooms[roompos].deck[j][1]) {
                        return true
                    }
                }
            }
        }

        function swapModes(roompos, command) {
            var fpos;
            var zpos;
            for (var i = 0; i < rooms[roompos].users.length; i++) {
                console.log('mode --->' + rooms[roompos].users[i].mode);
                console.log('User--->' + rooms[roompos].users[i].socketID);
                if (rooms[roompos].users[i].mode == 0) {
                    zpos = i;
                    console.log('zpos--->' + zpos);
                }
            }
            for (var i = 0; i < rooms[roompos].users.length; i++) {
                rooms[roompos].users[i].mode = 2;
                console.log('mode --->' + rooms[roompos].users[i].mode);
                console.log('User--->' + rooms[roompos].users[i].socketID);
            }

            if (zpos == 0) {
                fpos = rooms[roompos].users.length - 1;
                console.log('fpos--->' + fpos);
            }
            else {
                fpos = zpos - 1;
                console.log('fpos--->' + fpos);

            }
            if (command == 'take') {
                if (rooms[roompos].users.length == 2) {
                    if (zpos == rooms[roompos].users.length - 1) {
                        rooms[roompos].users[zpos].mode = 0;
                        rooms[roompos].users[zpos - 1].mode = 1;
                    }
                    else {
                        rooms[roompos].users[zpos].mode = 0;
                        rooms[roompos].users[zpos + 1].mode = 1;
                    }
                }
                if (rooms[roompos].users.length == 3) {
                    if (zpos == rooms[roompos].users.length - 1) {
                        rooms[roompos].users[0].mode = 1;
                        rooms[roompos].users[1].mode = 0;
                    }
                    else if (zpos == 1) {
                        rooms[roompos].users[0].mode = 0;
                        rooms[roompos].users[2].mode = 1;
                    } else if (zpos == 0) {
                        rooms[roompos].users[1].mode = 1;
                        rooms[roompos].users[2].mode = 0;
                    }
                }
                if (rooms[roompos].users.length == 4) {
                    console.log('Users count == 4')
                    if (zpos == rooms[roompos].users.length - 1) {
                        rooms[roompos].users[0].mode = 1;
                        rooms[roompos].users[1].mode = 0;
                    } else if (zpos == 1) {
                        rooms[roompos].users[2].mode = 1;
                        rooms[roompos].users[3].mode = 0;
                    } else if (zpos == 2) {
                        rooms[roompos].users[3].mode = 1;
                        rooms[roompos].users[0].mode = 0;
                    } else if (zpos == 0) {
                        rooms[roompos].users[1].mode = 1;
                        rooms[roompos].users[2].mode = 0;
                    }
                }
                console.log('After swap=---')
                for (var i = 0; i < rooms[roompos].users.length; i++) {
                    console.log('mode --->' + rooms[roompos].users[i].mode);
                    console.log('User--->' + rooms[roompos].users[i].socketID);
                    if (rooms[roompos].users[i].mode == 0) {
                        zpos = i;
                        console.log('zpos--->' + zpos);
                    }
                }
            }
            else if (command == 'pass') {
                if (zpos == rooms[roompos].users.length - 1) {
                    rooms[roompos].users[zpos].mode = 1;
                    console.log('User is attacking--->' + rooms[roompos].users[zpos].socketID);
                    console.log('User mode is--->' + rooms[roompos].users[zpos].mode);
                    rooms[roompos].users[0].mode = 0;
                    console.log('User is defending--->' + rooms[roompos].users[0].socketID);
                    console.log('User mode is--->' + rooms[roompos].users[0].mode);
                }
                else {
                    rooms[roompos].users[zpos].mode = 1;
                    console.log('User is attacking--->' + rooms[roompos].users[zpos].socketID);
                    console.log('User mode is--->' + rooms[roompos].users[zpos].mode);
                    rooms[roompos].users[zpos + 1].mode = 0;
                    console.log('User is defending--->' + rooms[roompos].users[zpos + 1].socketID);
                    console.log('User mode is--->' + rooms[roompos].users[zpos + 1].mode);
                }
            }
        }

        function checkVariants(roompos) {
            for (var i = 0; i < rooms[roompos].users.length; i++) {
                rooms[roompos].users[i].variants.length = 0;
                if (rooms[roompos].users[i].mode == 1) {
                    for (var j = 0; j < rooms[roompos].users[i].hand.length; j++) {
                        rooms[roompos].users[i].variants.push(1);
                    }

                } else if (rooms[roompos].users[i].mode == 0) {

                    for (var j = 0; j < rooms[roompos].users[i].hand.length; j++) {
                        var card = rooms[roompos].users[i].hand[j];
                        if (card[0] == rooms[roompos].trump && rooms[roompos].fight[0] != rooms[roompos].trump) {
                            rooms[roompos].users[i].variants.push(1);
                        } else if (card[0] == rooms[roompos].fight[0][0] && card[1] > rooms[roompos].fight[0][1]) {
                            rooms[roompos].users[i].variants.push(1);
                        } else {
                            rooms[roompos].users[i].variants.push(0);
                        }
                    }
                } else if (rooms[roompos].users[i].mode == 2) {
                    for (var j = 0; j < rooms[roompos].users[i].hand.length; j++) {
                        var card = rooms[roompos].users[i].hand[j];
                        for (var k = 0; k < rooms[roompos].deck.length; k++) {
                            if (card[1] == rooms[roompos].deck[k][1]) {
                                rooms[roompos].users.variants.push(1);
                            } else {
                                rooms[roompos].users.variants.push(0);
                            }

                        }
                    }
                }
            }
        }
    };
});

//rooms[roompos].winners.push(rooms[roompos].users[i])
//rooms[roompos].users.splice(i,1)
